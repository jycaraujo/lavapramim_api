/**
 * DeliveryPeople.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    precoKm: {
      type: 'integer',
    },
    description: {
      type: 'text'
    },
    status: {
      type: 'string',
      enum: [
        'online',
        'offline'
      ],
      defaultsTo: 'offline'
    },
    user: {
      model: 'user',
      unique: true,
    },
    orders: {
      collection: 'deliveryorder',
      via: 'deliveryPeople'
    }
  }
};

