/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

// We don't want to store password with out encryption
var bcrypt = require('bcrypt');

module.exports = {
  schema: true,

  attributes: {
    firstName: {
      type: 'string',
    },
    lastName: {
      type: 'string',
    },
    email: {
      type: 'string',
      required: true,
      unique: true
    },
    password: {
      type: 'string',
      required: true,
    },
    cpf: {
      type: 'string',
    },
    userType: {
      type: "string",
      enum: ['client', 'deliverypeople', 'laundry', 'washpeople']
    },
    isOnline: {
      type: 'boolean',
      defaultsTo: 'false',
    },

    orders: {
      collection: 'order',
      via: 'owner',
    },

    // Add a reference to address
    address: {
      collection: 'useraddress',
      via: 'user'
    },

    washPeople: {
      collection: 'washpeople',
      via: 'user',
    },

    deliveryPeople: {
      collection: 'deliverypeople',
      via: 'user'
    },

    getFullName: function () {
      return this.firstName + ' ' + this.lastName;
    },
  },
  // We don't wan't to send back encrypted password either
  toJSON: function () {
    var obj = this.toObject();
    delete obj.password;
    return obj;
  },
  // Here we encrypt password before creating a User
  beforeCreate: function (values, next) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) return next(err);
      bcrypt.hash(values.password, salt, function (err, hash) {
        if (err) return next(err);
        values.password = hash;
        next();
      })
    })
  },
  comparePassword: function (password, user, cb) {
    bcrypt.compare(password, user.password, function (err, match) {
      if (err) cb(err);
      if (match) {
        cb(null, true);
      } else {
        cb(err);
      }
    })
  },

};

