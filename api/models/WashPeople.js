/**
 * WashPeople.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    companyName: {
      type: 'string',
    },
    cnpj: {
      type: 'string',
    },
    serviceDescription: {
      type: "text",
    },
    user: {
      model: 'user',
      unique: true,
    },
    status: {
      type: 'string',
      enum: [
        'online',
        'offline'
      ],
      defaultsTo: 'offline',
    },
    priceByBasket: {
      type: 'float'
    }
  },
};

