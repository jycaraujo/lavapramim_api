/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  getLaundries: function(req, res){
    User.find()
      .where({userType: ['laundry', 'washPeople']})
      .populate('washPeople')
      .populate('address')
      .exec(function callBack(err,results){
        if (err) {
          console.error(err)
          return res.json(err.status, {err: err});
        }
        res.json(200, {users: results});
      });
  },
  getDelivery: function(req, res){
    User.find()
      .where({userType: 'deliverypeople'})
      .populate('deliveryPeople')
      .populate('address')
      .exec(function callBack(err,results){
        if (err) {
          console.error(err)
          return res.json(err.status, {err: err});
        }
        res.json(200, {users: results});
      });
  },
  create: function (req, res) {
    console.log(req)
    if (req.body.password !== req.body.confirmPassword) {
      return res.json(401, {err: 'Password doesn\'t match, What a shame!'});
    }
    User.create(req.body).exec(function (err, user) {
      if (err) {
        console.error(err)
        return res.json(err.status, {err: err});
      }
      // If user created successfuly we return user and token as response
      if (user) {
        // NOTE: payload is { id: user.id}
        res.json(200, {user: user, token: jwToken.issue({id: user.id})});
      }
    });
  },
};
