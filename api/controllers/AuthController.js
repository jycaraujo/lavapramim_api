/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  index: function (req, res) {
    console.log(req)
    var email = req.param('email');
    var password = req.param('password');
    var remember = req.param('remember');

    if (!email || !password) {
      return res.json(401, {err: 'email and password required'});
    }

    User.findOne({email: email}, function (err, user) {
      if (!user) {
        return res.json(401, {err: 'invalid email or password'});
      }

      User.comparePassword(password, user, function (err, valid) {
        if (err) {
          return res.json(403, {err: 'forbidden'});
        }

        if (!valid) {
          return res.json(401, {err: 'invalid email or password'});
        } else {
          // if(remember==true)
          //   req.session.me = user;
          console.log(req)
          user.isOnline = true;
          user.save();
          res.json({
            user: user,
            token: jwToken.issue({id: user.id})
          });
        }
      });
    })
  },


};

